# abm-credit


## 1. Install 

**Clone project**

**Create database**
```
Create database abm-credit
```

**Copy wp-config-sample.php to wp-config.php**

Change config Database
```
   /** The name of the database for WordPress */
   define( 'DB_NAME', 'abm-credit' );
   
   /** MySQL database username */
   define( 'DB_USER', 'root' );
   
   /** MySQL database password */
   define( 'DB_PASSWORD', '' );
   
   /** MySQL hostname */
   define( 'DB_HOST', 'localhost' );
```

## 2. Manage 

**Account login**
```
Go to : /wp-admin
```

**Import media uploads**

```