<?php
/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version		1.0.8
 * 
 * Blog Post Standard Post Format Template
 * Created by CMSMasters
 * 
 */
 
 
$cmsmasters_option = payday_loans_get_global_options();


$cmsmasters_post_title = get_post_meta(get_the_ID(), 'cmsmasters_post_title', true);

$cmsmasters_post_image_show = get_post_meta(get_the_ID(), 'cmsmasters_post_image_show', true);

list($cmsmasters_layout) = payday_loans_theme_page_layout_scheme();

if ($cmsmasters_layout == 'fullwidth') {
	$cmsmasters_image_thumb_size = 'cmsmasters-full-masonry-thumb';
} else {
	$cmsmasters_image_thumb_size = 'cmsmasters-masonry-thumb';
}

?>

<!--_________________________ Start Standard Article _________________________ -->

<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_open_post'); ?>>
	<?php 
	if (
		$cmsmasters_option['payday-loans' . '_blog_post_like'] || 
		$cmsmasters_option['payday-loans' . '_blog_post_comment'] || 
		$cmsmasters_option['payday-loans' . '_blog_post_date']
	) {
		echo '<div class="cmsmasters_open_post_info">';
		
			payday_loans_get_post_date('post');
			
			if (
				$cmsmasters_option['payday-loans' . '_blog_post_like'] || 
				$cmsmasters_option['payday-loans' . '_blog_post_comment']
			) {
				echo '<div class="cmsmasters_open_post_info_bot">';
				
					payday_loans_get_post_likes('post');
			
					payday_loans_get_post_comments('post');
				
				echo '</div>';
			}
			
		echo '</div>';
	}
		
	echo '<div class="cmsmasters_open_post_wrap">'; 
		echo '<div class="cmsmasters_open_post_inner">';
			if (!post_password_required() && has_post_thumbnail() && $cmsmasters_post_image_show != 'true') {
				echo '<div class="cmsmasters_post_cont_img_wrap">';
				
					payday_loans_thumb(get_the_ID(), $cmsmasters_image_thumb_size, false, 'img_' . get_the_ID(), false, false, false, true, false);
					
				echo '</div>';
			}
		
		if ($cmsmasters_post_title == 'true') {
			payday_loans_post_title_nolink(get_the_ID(), 'h2');
		}
		
		
		if (
			$cmsmasters_option['payday-loans' . '_blog_post_author'] || 
			$cmsmasters_option['payday-loans' . '_blog_post_cat'] 
		) {
			echo '<div class="cmsmasters_post_cont_info entry-meta">';
				
				payday_loans_get_post_author('post');
				
				payday_loans_get_post_category(get_the_ID(), 'category', 'post');
				
			echo '</div>';
		}
		
		
		if (get_the_content() != '') {
			echo '<div class="cmsmasters_post_content entry-content">';
				
				the_content();
				
				
				wp_link_pages(array( 
					'before' => '<div class="subpage_nav" role="navigation">' . '<strong>' . esc_html__('Pages', 'payday-loans') . ':</strong>', 
					'after' => '</div>', 
					'link_before' => ' [ ', 
					'link_after' => ' ] ' 
				));
				
			echo '</div>';
		}
		
		
		if (
			$cmsmasters_option['payday-loans' . '_blog_post_tag'] 
		) {
			echo '<footer class="cmsmasters_post_footer entry-meta">';
				
				payday_loans_get_post_tags();
				
			echo '</footer>';
		}
		
	echo '</div>' . 
		'</div>';
	?>
</article>
<!--_________________________ Finish Standard Article _________________________ -->

