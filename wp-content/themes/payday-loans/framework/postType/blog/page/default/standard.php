<?php
/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version		1.0.3
 * 
 * Blog Page Default Standard Post Format Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_post_metadata = !is_home() ? explode(',', $cmsmasters_metadata) : array();


$date = (in_array('date', $cmsmasters_post_metadata) || is_home()) ? true : false;
$categories = (get_the_category() && (in_array('categories', $cmsmasters_post_metadata) || is_home())) ? true : false;
$author = (in_array('author', $cmsmasters_post_metadata) || is_home()) ? true : false;
$comments = (comments_open() && (in_array('comments', $cmsmasters_post_metadata) || is_home())) ? true : false;
$likes = (in_array('likes', $cmsmasters_post_metadata) || is_home()) ? true : false;
$more = (in_array('more', $cmsmasters_post_metadata) || is_home()) ? true : false;

?>

<!--_________________________ Start Standard Article _________________________ -->

<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_post_default'); ?>>
	<?php 
	if ($date || $likes || $comments) {
		echo '<div class="cmsmasters_post_info entry-meta">';
			
			if (is_sticky()) {
				echo '<div class="cmsmasters_sticky cmsmasters_theme_icon_sticky"></div>';
			}
			
			$date ? payday_loans_get_post_date('page', 'default') : '';
			
			if ($likes || $comments) {
				echo '<div class="cmsmasters_post_info_bot">';
				
					$likes ? payday_loans_get_post_likes('page') : '';
			
					$comments ? payday_loans_get_post_comments('page') : '';
					
				echo '</div>';
			}
			
		echo '</div>';
	}
	?>
	<div class="cmsmasters_post_cont">
		<?php
		if (!post_password_required() && has_post_thumbnail()) {
			echo '<div class="cmsmasters_post_cont_img_wrap">';
			
				payday_loans_thumb(get_the_ID(), 'post-thumbnail', true, false, false, false, false, true, false);
				
			echo '</div>';
		}
		
		echo '<div class="cmsmasters_post_cont_inner_wrap">';
		
			payday_loans_post_heading(get_the_ID(), 'h2');
			
			
			if ($author || $categories) {
				echo '<div class="cmsmasters_post_cont_info entry-meta">';
					
					$author ? payday_loans_get_post_author('page') : '';
					
					$categories ? payday_loans_get_post_category(get_the_ID(), 'category', 'page') : '';
					
				echo '</div>';
			}
			
			
			payday_loans_post_exc_cont();
			
			
			if ($more) {
				echo '<footer class="cmsmasters_post_footer entry-meta">';
					
					$more ? payday_loans_post_more(get_the_ID()) : '';
					
				echo '</footer>';
			}
			
		echo '</div>';
		?>
	</div>
</article>
<!--_________________________ Finish Standard Article _________________________ -->

