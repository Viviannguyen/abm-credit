<?php
/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version		1.0.3
 * 
 * Blog Page Default Gallery Post Format Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_post_metadata = !is_home() ? explode(',', $cmsmasters_metadata) : array();


$date = (in_array('date', $cmsmasters_post_metadata) || is_home()) ? true : false;
$categories = (get_the_category() && (in_array('categories', $cmsmasters_post_metadata) || is_home())) ? true : false;
$author = (in_array('author', $cmsmasters_post_metadata) || is_home()) ? true : false;
$comments = (comments_open() && (in_array('comments', $cmsmasters_post_metadata) || is_home())) ? true : false;
$likes = (in_array('likes', $cmsmasters_post_metadata) || is_home()) ? true : false;
$more = (in_array('more', $cmsmasters_post_metadata) || is_home()) ? true : false;


$cmsmasters_post_images = explode(',', str_replace(' ', '', str_replace('img_', '', get_post_meta(get_the_ID(), 'cmsmasters_post_images', true))));


$uniqid = uniqid();

?>

<!--_________________________ Start Gallery Article _________________________ -->

<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_post_default'); ?>>
	<?php 
	if ($date || $likes || $comments) {
		echo '<div class="cmsmasters_post_info entry-meta">';
		
			if (is_sticky()) {
				echo '<div class="cmsmasters_sticky cmsmasters_theme_icon_sticky"></div>';
			}
			
			$date ? payday_loans_get_post_date('page', 'default') : '';
			
			if ($likes || $comments) {
				echo '<div class="cmsmasters_post_info_bot">';
				
					$likes ? payday_loans_get_post_likes('page') : '';
			
					$comments ? payday_loans_get_post_comments('page') : '';
					
				echo '</div>';
			}
			
		echo '</div>';
	}
	?>
	<div class="cmsmasters_post_cont">
		<?php
		if (!post_password_required()) {
		
			if (sizeof($cmsmasters_post_images) > 1 || sizeof($cmsmasters_post_images) == 1 && $cmsmasters_post_images[0] != '' || has_post_thumbnail()) {
			
				echo '<div class="cmsmasters_post_cont_img_wrap">';
			
				if (sizeof($cmsmasters_post_images) > 1) {
				?>
						<script type="text/javascript">
							jQuery(document).ready(function () {
								jQuery('.cmsmasters_slider_<?php echo esc_attr($uniqid); ?>').owlCarousel( { 
									singleItem : 		true, 
									transitionStyle : 	false, 
									rewindNav : 		true, 
									slideSpeed : 		200, 
									paginationSpeed : 	800, 
									rewindSpeed : 		1000, 
									autoPlay : 			false, 
									stopOnHover : 		false, 
									pagination : 		true, 
									navigation : 		false, 
									navigationText : 	[ 
										'<span></span>', 
										'<span></span>' 
									] 
								} );
							} );
						</script>
						<div id="cmsmasters_owl_carousel_<?php the_ID(); ?>" class="cmsmasters_slider_<?php echo esc_attr($uniqid); ?> cmsmasters_owl_slider">
						<?php 
							foreach ($cmsmasters_post_images as $cmsmasters_post_image) {
								$image_atts = wp_prepare_attachment_for_js(strstr($cmsmasters_post_image, '|', true));
								
								
								echo '<div>' . 
									'<figure>' . 
										wp_get_attachment_image(strstr($cmsmasters_post_image, '|', true), 'post-thumbnail', false, array( 
											'class' => 	'full-width', 
											'alt' => ($image_atts['alt'] != '') ? esc_attr($image_atts['alt']) : cmsmasters_title(get_the_ID(), false), 
											'title' => ($image_atts['title'] != '') ? esc_attr($image_atts['title']) : cmsmasters_title(get_the_ID(), false) 
										)) . 
									'</figure>' . 
								'</div>';
							}
						?>
						</div>
					<?php 
					} elseif (sizeof($cmsmasters_post_images) == 1 && $cmsmasters_post_images[0] != '') {
						payday_loans_thumb(get_the_ID(), 'post-thumbnail', false, 'img_' . get_the_ID(), true, true, true, true, $cmsmasters_post_images[0]);
					} elseif (has_post_thumbnail()) {
						payday_loans_thumb(get_the_ID(), 'post-thumbnail', false, 'img_' . get_the_ID(), true, true, true, true, false);
					}
					
				echo '</div>';
				
			}
			
		}
		
		
		echo '<div class="cmsmasters_post_cont_inner_wrap">';
		
			payday_loans_post_heading(get_the_ID(), 'h2');
			
			
			if ($author || $categories) {
				echo '<div class="cmsmasters_post_cont_info entry-meta">';
					
					$author ? payday_loans_get_post_author('page') : '';
					
					$categories ? payday_loans_get_post_category(get_the_ID(), 'category', 'page') : '';
					
				echo '</div>';
			}
			
			
			payday_loans_post_exc_cont();
			
			
			if ($more) {
				echo '<footer class="cmsmasters_post_footer entry-meta">';
					
					$more ? payday_loans_post_more(get_the_ID()) : '';
					
				echo '</footer>';
			}
			
		echo '</div>';
		?>
	</div>
</article>
<!--_________________________ Finish Gallery Article _________________________ -->

