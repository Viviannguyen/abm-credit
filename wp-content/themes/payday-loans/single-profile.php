<?php
/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version		1.0.7
 * 
 * Single Profile Template
 * Created by CMSMasters
 * 
 */


get_header();


$cmsmasters_option = payday_loans_get_global_options();


$cmsmasters_profile_sharing_box = get_post_meta(get_the_ID(), 'cmsmasters_profile_sharing_box', true);


echo '<!--_________________________ Start Content _________________________ -->' . "\n" . 
'<div class="middle_content entry">';


if (have_posts()) : the_post();
	echo '<div class="profiles opened-article">' . "\n";
	
	
	get_template_part('framework/postType/profile/post/standard');

	
	if ($cmsmasters_option['payday-loans' . '_profile_post_nav_box']) {
		payday_loans_prev_next_posts();
	}
	
	
	if ($cmsmasters_profile_sharing_box == 'true') {
		payday_loans_sharing_box(esc_html__('Like this profile?', 'payday-loans'), 'h3');
	}
	
	
	comments_template(); 
	
	
	echo '</div>';
endif;


echo '</div>' . "\n" . 
'<!-- _________________________ Finish Content _________________________ -->' . "\n\n";


get_footer();

