/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version 	1.1.1
 * 
 * Theme Information
 * Created by CMSMasters
 * 
 */

To update your theme please use the files list from the FILE LOGS below and substitute/add the listed files on your server with the same files in the Updates folder.

Important: after you have updated the theme, in your admin panel please proceed to
Theme Settings - Fonts and click "Save" in any tab,
then proceed to 
Theme Settings - Colors and click "Save" in any tab here.
--------------------------------------
Version 1.1.1: files operations:
	Theme Files edited:
		payday-loans\framework\admin\inc\plugin-activator.php
		payday-loans\readme.txt
		payday-loans\style.css

		Proceed to wp-content\plugins\cmsmasters-content-composer
		and update all files in this folder to version 1.7.4.2
	
		Proceed to wp-content\plugins\revslider
		and update all files in this folder to version 6.3.3


--------------------------------------
Version 1.0.9-1.1.0: files operations:
	Theme Files edited:
		payday-loans\css\less\style.less
		payday-loans\css\style.css
		payday-loans\framework\admin\inc\js\jquery.cmsmastersLightbox.js
		payday-loans\framework\admin\inc\js\wp-color-picker-alpha.js
		payday-loans\framework\admin\inc\plugin-activator.php
		payday-loans\framework\class\class-tgm-plugin-activation.php
		payday-loans\framework\function\template-functions.php
		payday-loans\framework\function\theme-colors-primary.php
		payday-loans\gutenberg\cmsmasters-module-functions.php
		payday-loans\gutenberg\css\editor-style.css
		payday-loans\gutenberg\css\frontend-style.css
		payday-loans\gutenberg\css\less\module-style.less
		payday-loans\gutenberg\function\module-colors.php
		payday-loans\gutenberg\function\module-fonts.php
		payday-loans\header.php
		payday-loans\js\jquery.iLightBox.min.js
		payday-loans\js\jquery.isotope.min.js
		payday-loans\js\jquery.isotope.mode.js
		payday-loans\readme.txt
		payday-loans\style.css

		Proceed to wp-content\plugins\cmsmasters-content-composer
		and update all files in this folder to version 1.7.4.1
	
		Proceed to wp-content\plugins\revslider
		and update all files in this folder to version 6.2.23
		
		Proceed to wp-content\plugins\LayerSlider
		and update all files in this folder to version 6.11.22


--------------------------------------
Version 1.0.8: files operations:
	Theme Files edited:
		payday-loans\cmsmasters-c-c\shortcodes\cmsmasters-counter.php
		payday-loans\cmsmasters-c-c\shortcodes\cmsmasters-custom-heading.php
		payday-loans\cmsmasters-c-c\shortcodes\cmsmasters-featured-block.php
		payday-loans\cmsmasters-c-c\shortcodes\cmsmasters-posts-slider.php
		payday-loans\cmsmasters-c-c\shortcodes\cmsmasters-pricing-table-item.php
		payday-loans\cmsmasters-c-c\shortcodes\cmsmasters-stat.php
		payday-loans\cmsmasters-c-c\shortcodes\cmsmasters-tab.php
		payday-loans\comments.php
		payday-loans\css\adaptive.css
		payday-loans\css\less\adaptive.less
		payday-loans\css\less\style.less
		payday-loans\framework\admin\inc\js\admin-theme-scripts.js
		payday-loans\framework\admin\inc\plugin-activator.php
		payday-loans\framework\admin\options\cmsmasters-theme-options.php
		payday-loans\framework\admin\options\css\cmsmasters-theme-options.css
		payday-loans\framework\admin\settings\cmsmasters-theme-settings.php
		payday-loans\framework\admin\settings\inc\cmsmasters-helper-functions.php
		payday-loans\framework\class\Browser.php
		payday-loans\framework\class\widgets.php
		payday-loans\framework\function\breadcrumbs.php
		payday-loans\framework\function\likes.php
		payday-loans\framework\function\template-functions-post.php
		payday-loans\framework\function\template-functions-profile.php
		payday-loans\framework\function\template-functions-project.php
		payday-loans\framework\function\template-functions-shortcodes.php
		payday-loans\framework\function\template-functions.php
		payday-loans\framework\function\theme-colors-primary.php
		payday-loans\framework\function\theme-fonts.php
		payday-loans\framework\function\theme-functions.php
		payday-loans\framework\postType\blog\post\gallery.php
		payday-loans\framework\postType\blog\post\image.php
		payday-loans\framework\postType\blog\post\standard.php
		payday-loans\framework\postType\blog\post\video.php
		payday-loans\functions.php
		payday-loans\js\jquery.script.js
		payday-loans\sidebar-bottom.php
		payday-loans\sidebar.php
		payday-loans\style.css
		payday-loans\gutenberg\function\module-colors.php
		payday-loans\gutenberg\css\frontend-style.css
		payday-loans\gutenberg\css\editor-style.css
		payday-loans\framework\admin\inc\plugins\revslider.zip
		payday-loans\framework\admin\inc\plugins\LayerSlider.zip
		payday-loans\framework\admin\inc\plugins\cmsmasters-mega-menu.zip
		payday-loans\framework\admin\inc\plugins\cmsmasters-importer.zip
		payday-loans\framework\admin\inc\plugins\cmsmasters-content-composer.zip
		payday-loans\framework\admin\inc\plugins\cmsmasters-contact-form-builder.zip
		payday-loans\framework\admin\inc\demo-content\main\widgets.json
		payday-loans\framework\admin\inc\demo-content\main\theme-settings.txt
		payday-loans\framework\admin\inc\demo-content\main\content.xml
		payday-loans\framework\admin\inc\demo-content-importer.php
		payday-loans\css\style.css
		payday-loans\css\less\general.less

	Theme Files added:
		payday-loans\css\less\general.less
		payday-loans\css\style.css
		payday-loans\framework\admin\inc\demo-content-importer.php
		payday-loans\framework\admin\inc\demo-content\main\content.xml
		payday-loans\framework\admin\inc\demo-content\main\theme-settings.txt
		payday-loans\framework\admin\inc\demo-content\main\widgets.json
		payday-loans\framework\admin\inc\plugins\cmsmasters-importer.zip
		payday-loans\gutenberg\cmsmasters-module-functions.php
		payday-loans\gutenberg\css\editor-style.css
		payday-loans\gutenberg\css\frontend-style.css
		payday-loans\gutenberg\css\less\editor-style.less
		payday-loans\gutenberg\css\less\frontend-style.less
		payday-loans\gutenberg\css\less\module-style.less
		payday-loans\gutenberg\css\module-rtl.css
		payday-loans\gutenberg\function\module-colors.php
		payday-loans\gutenberg\function\module-fonts.php
		payday-loans\gutenberg\js\editor-options.js

	Theme Files deleted:
		payday-loans\framework\class\twitteroauth.php
		payday-loans\framework\class\OAuth.php

		Proceed to wp-content\plugins\cmsmasters-content-composer
		and update all files in this folder to version 1.7.4
		
		Proceed to wp-content\plugins\cmsmasters-mega-menu
		and update all files in this folder to version 1.2.7
	
		Proceed to wp-content\plugins\revslider
		and update all files in this folder to version 5.4.8.1


--------------------------------------
Version 1.0.7: files operations:

	Theme Files edited:
		payday-loans\comments.php
		payday-loans\css\adaptive.css
		payday-loans\css\less\adaptive.less
		payday-loans\css\less\style.less
		payday-loans\footer.php
		payday-loans\framework\admin\inc\plugin-activator.php
		payday-loans\framework\admin\inc\plugins\cmsmasters-contact-form-builder.zip
		payday-loans\framework\admin\inc\plugins\cmsmasters-content-composer.zip
		payday-loans\framework\admin\inc\plugins\cmsmasters-mega-menu.zip
		payday-loans\framework\admin\inc\plugins\envato-market.zip
		payday-loans\framework\admin\inc\plugins\LayerSlider.zip
		payday-loans\framework\admin\inc\plugins\revslider.zip
		payday-loans\framework\admin\settings\cmsmasters-theme-settings-general.php
		payday-loans\framework\class\widgets.php
		payday-loans\js\jquery.isotope.mode.js
		payday-loans\js\jquery.script.js
		payday-loans\readme.txt
		payday-loans\style.css
		
		
--------------------------------------
Version 1.0.6: files operations:

	Theme Files edited:
		payday-loans\framework\admin\inc\js\wp-color-picker-alpha.js
		payday-loans\framework\admin\inc\plugin-activator.php
		payday-loans\framework\admin\inc\plugins\cmsmasters-contact-form-builder.zip
		payday-loans\framework\admin\inc\plugins\cmsmasters-content-composer.zip
		payday-loans\framework\admin\inc\plugins\LayerSlider.zip
		payday-loans\framework\admin\inc\plugins\revslider.zip
		payday-loans\framework\function\theme-functions.php
		payday-loans\framework\admin\settings\cmsmasters-theme-settings-general.php
		payday-loans\js\jquery.script.js
		payday-loans\framework\languages\payday-loans.pot
		payday-loans\js\jqueryLibraries.min.js
		payday-loans\readme.txt
		payday-loans\style.css
	
	Theme Files added:
		payday-loans\framework\admin\inc\plugins\envato-market.zip
		payday-loans\js\scrollspy.js
		
	Theme Files updated:

		Proceed to wp-content\plugins\cmsmasters-content-composer
		and update all files in this folder to version 1.6.7
		
		Proceed to wp-content\plugins\LayerSlider
		and update all files in this folder to version 6.6.1
	
		Proceed to wp-content\plugins\cmsmasters-contact-form-builder
		and update all files in this folder to version 1.3.9
		
		Proceed to wp-content\plugins\revslider
		and update all files in this folder to version 5.4.6.2
		
		
--------------------------------------
Version 1.0.5: files operations:

	Theme Files edited:
		payday-loans\framework\admin\inc\plugin-activator.php
		payday-loans\framework\admin\inc\plugins\cmsmasters-contact-form-builder.zip
		payday-loans\framework\admin\inc\plugins\cmsmasters-content-composer.zip
		payday-loans\framework\admin\inc\plugins\LayerSlider.zip
		payday-loans\framework\admin\inc\plugins\revslider.zip
		payday-loans\framework\function\theme-functions.php
		payday-loans\js\jquery.script.js
		payday-loans\js\jqueryLibraries.min.js
		payday-loans\readme.txt
		payday-loans\style.css
		
	Theme Files updated:

		Proceed to wp-content\plugins\cmsmasters-content-composer
		and update all files in this folder to version 1.6.5
		
		Proceed to wp-content\plugins\LayerSlider
		and update all files in this folder to version 6.5.7
	
		Proceed to wp-content\plugins\cmsmasters-contact-form-builder
		and update all files in this folder to version 1.3.8
		
		Proceed to wp-content\plugins\revslider
		and update all files in this folder to version 5.4.5.1
		
		
--------------------------------------
Version 1.0.4: files operations:

	Theme Files edited:
		payday-loans\css\adaptive.css
		payday-loans\css\less\adaptive.less
		payday-loans\css\less\style.less
		payday-loans\footer.php
		payday-loans\framework\admin\inc\plugin-activator.php
		payday-loans\framework\admin\inc\plugins\cmsmasters-content-composer.zip
		payday-loans\framework\admin\inc\plugins\LayerSlider.zip
		payday-loans\framework\admin\inc\plugins\revslider.zip
		payday-loans\framework\admin\settings\cmsmasters-theme-settings-general.php
		payday-loans\framework\function\theme-functions.php
		payday-loans\framework\postType\blog\post\standard.php
		payday-loans\functions.php
		payday-loans\js\jquery.isotope.mode.js
		payday-loans\js\jquery.script.js
		payday-loans\js\jqueryLibraries.min.js
		payday-loans\readme.txt
		payday-loans\single-project.php
		payday-loans\style.css
		
	Theme Files updated:

		Proceed to wp-content\plugins\cmsmasters-content-composer
		and update all files in this folder to version 1.6.4
		
		Proceed to wp-content\plugins\LayerSlider
		and update all files in this folder to version 6.1.6
		
		Proceed to wp-content\plugins\revslider
		and update all files in this folder to version 5.4.1
		
		
--------------------------------------
Version 1.0.3: files operations:

	Theme Files edited:
		payday-loans\css\adaptive.css
		payday-loans\style.css
		payday-loans\cmsmasters-c-c\js\cmsmasters-c-c-shortcodes-extend.js
		payday-loans\framework\admin\options\js\cmsmasters-theme-options-toggle.js
		payday-loans\css\less\adaptive.less
		payday-loans\css\less\style.less
		payday-loans\cmsmasters-c-c\filters\cmsmasters-c-c-atts-filters.php
		payday-loans\framework\admin\inc\plugin-activator.php
		payday-loans\framework\admin\options\cmsmasters-theme-options-post.php
		payday-loans\framework\function\template-functions.php
		payday-loans\framework\function\theme-functions.php
		payday-loans\framework\postType\blog\page\default\gallery.php
		payday-loans\framework\postType\blog\page\default\image.php
		payday-loans\framework\postType\blog\page\default\standard.php
		payday-loans\framework\postType\blog\page\masonry\gallery.php
		payday-loans\framework\postType\blog\page\timeline\gallery.php
		payday-loans\framework\postType\blog\post\gallery.php
		payday-loans\framework\postType\blog\post\image.php
		payday-loans\framework\postType\blog\post\standard.php
		payday-loans\framework\postType\portfolio\post\gallery.php
		payday-loans\framework\postType\portfolio\post\standard.php
		payday-loans\framework\postType\quote\grid.php
		payday-loans\framework\postType\quote\slider.php
		payday-loans\framework\languages\payday-loans.pot
		payday-loans\readme.txt




	Theme Files deleted:

		payday-loans\cmsmasters-c-c\shortcodes\sc-name.php
	
	Theme Files added:
	
		payday-loans\css\fonts\fontello-custom.woff2
		
	Theme Files updated:

		Proceed to wp-content\plugins\cmsmasters-content-composer
		and update all files in this folder to version 1.6.2
		
		Proceed to wp-content\plugins\LayerSlider
		and update all files in this folder to version 6.1.0
		
		Proceed to wp-content\plugins\revslider
		and update all files in this folder to version 5.3.1.5
		
		
--------------------------------------
Version 1.0.2: files operations:

	Theme Files edited:
		payday-loans\css\less\style.less
		payday-loans\framework\admin\inc\plugin-activator.php
		payday-loans\framework\admin\inc\plugins\cmsmasters-content-composer.zip
		payday-loans\framework\admin\settings\js\cmsmasters-theme-settings-toggle.js
		payday-loans\style.css
		payday-loans\readme.txt
		payday-loans\archive.php
		payday-loans\author.php
		payday-loans\framework\function\pagination.php
		payday-loans\index.php
		payday-loans\search.php
		payday-loans\framework\admin\settings\js\cmsmasters-theme-settings.js
		payday-loans\framework\function\likes.php
		payday-loans\framework\function\template-functions.php
		payday-loans\framework\function\theme-functions.php
		payday-loans\framework\languages\payday-loans.pot
		payday-loans\functions.php
		payday-loans\js\jquery.script.js
		
		
	Theme Files deleted:

		payday-loans\framework\class\likes-posttype.php
	
	Theme Files added:
	
		payday-loans\css\styles\payday-loans.css
		payday-loans\css\styles\payday-loans_colors_primary.css
		payday-loans\css\styles\payday-loans_colors_secondary.css
		payday-loans\css\styles\payday-loans_fonts.css
		
	Theme Files updated:

		Proceed to wp-content\plugins\cmsmasters-content-composer
		and update all files in this folder to version 1.6.0

--------------------------------------
Version 1.0.1: files operations:

	Theme Files edited:
		payday-loans\framework\admin\inc\plugins\cmsmasters-content-composer.zip
		payday-loans\framework\admin\options\js\cmsmasters-theme-options.js
		payday-loans\readme.txt
		payday-loans\style.css

--------------------------------------
Version 1.0: Release!

