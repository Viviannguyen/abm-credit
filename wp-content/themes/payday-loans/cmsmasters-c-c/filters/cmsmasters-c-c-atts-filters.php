<?php
/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version 	1.0.3
 * 
 * Content Composer Attributes Filters
 * Created by CMSMasters
 * 
 */




// Special Heading Shortcode Attributes Filter
add_filter('cmsmasters_custom_heading_atts_filter', 'cmsmasters_custom_heading_atts');

function cmsmasters_custom_heading_atts() {
	return array( 
		'type' => 					'h1', 
		'font_family' => 			'', 
		'font_size' => 				'', 
		'line_height' => 			'', 
		'font_weight' => 			'400', 
		'font_style' => 			'normal', 
		'icon' => 					'', 
		'text_align' => 			'left', 
		'color' => 					'', 
		'bg_color' => 				'', 
		'link' => 					'', 
		'target' => 				'', 
		'margin_top' => 			'0', 
		'margin_bottom' => 			'0', 
		'border_radius' => 			'', 
		'divider' => 				'', 
		'divider_type' => 			'short', 
		'divider_height' => 		'1', 
		'divider_style' => 			'solid', 
		'divider_color' => 			'', 
		'custom_check' =>  			'', 
		'width_monitor' =>  		'767', 
		'custom_font_size' => 		'', 
		'custom_line_height' => 	'', 
		'animation' => 				'', 
		'animation_delay' => 		'', 
		'classes' => 				'' 
	);
}



// Posts Slider Shortcode Attributes Filter
add_filter('cmsmasters_posts_slider_atts_filter', 'cmsmasters_posts_slider_atts');

function cmsmasters_posts_slider_atts() {
	return array( 
		'orderby' => 				'date', 
		'order' => 					'DESC', 
		'post_type' => 				'post', 
		'blog_categories' => 		'', 
		'portfolio_categories' => 	'', 
		'columns' => 				'4', 
		'controls' =>				'', 
		'amount' => 				'1', 
		'count' => 					'12', 
		'pause' => 					'5', 
		'blog_metadata' => 			'title,date,categories,comments,likes,more', 
		'portfolio_metadata' => 	'title,categories,likes', 
		'animation' => 				'', 
		'animation_delay' => 		'', 
		'classes' => 				''
	);
}



// Featured Block Attributes Filter
add_filter('cmsmasters_featured_block_atts_filter', 'cmsmasters_featured_block_atts');

function cmsmasters_featured_block_atts() {
	return array( 
		'text_width' => 		'100', 
		'text_position' => 		'center', 
		'text_padding' => 		'', 
		'text_align' => 		'left', 
		'color_overlay' => 		'', 
		'fb_bg_color' => 		'', 
		'bg_img' => 			'', 
		'bg_position' => 		'', 
		'bg_repeat' => 			'', 
		'bg_attachment' => 		'', 
		'bg_size' => 			'', 
		'top_padding' => 		'', 
		'bottom_padding' => 	'', 
		'border_radius' => 		'', 
		'bd_color' =>	 		'', 
		'bd_width' =>	 		'', 
		'animation' => 			'', 
		'animation_delay' => 	'', 
		'classes' => 			'' 
	);
}



/* Register Admin Panel JS Scripts */
function payday_loans_register_admin_js_scripts() {
	global $pagenow;
	
	
	if ( 
		$pagenow == 'post-new.php' || 
		($pagenow == 'post.php' && isset($_GET['post']) && get_post_type($_GET['post']) != 'attachment') 
	) {
		wp_enqueue_script('composer-shortcodes-extend', get_template_directory_uri() . '/cmsmasters-c-c/js/cmsmasters-c-c-shortcodes-extend.js', array('cmsmasters_composer_shortcodes_js'), '1.0.0', true);
		
		wp_localize_script('composer-shortcodes-extend', 'composer_shortcodes_extend', array( 
			'translate_name_1'							=>		esc_attr__('Translate value 1', 'payday-loans'), 
			'translate_name_2' 							=>		esc_attr__('Translate value 2', 'payday-loans'), 
			'posts_slider_controls_enable_title'		=> 		esc_attr__('Controls', 'payday-loans'), 
			'cmsmasters_featured_block_field_bd_color' => 	esc_attr__('Border Color', 'payday-loans'), 
			'cmsmasters_featured_block_field_bd_width' => 	esc_attr__('Border Width', 'payday-loans'),
			'heading_field_custom_check' => 		esc_attr__('Set Custom Font Size for Small Screens', 'payday-loans'), 
			'heading_field_width_monitor' => 		esc_attr__('Monitor Width', 'payday-loans'), 
			'heading_field_custom_font_size' => 	esc_attr__('Custom Font Size', 'payday-loans'), 
			'heading_field_size_zero_note' => 		esc_attr__('number, in pixels (default value if empty or 0)', 'payday-loans'), 
			'heading_field_custom_line_height' => 	esc_attr__('Custom Line Height', 'payday-loans') 
		));
	}
}

add_action('admin_enqueue_scripts', 'payday_loans_register_admin_js_scripts');

